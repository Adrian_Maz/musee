<?php
/**
 * museum functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package museum
 */

if ( ! function_exists( 'museum_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function museum_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on museum, use a find and replace
	 * to change 'museum' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'museum', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'museum' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'museum_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'museum_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function museum_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'museum_content_width', 640 );
}
add_action( 'after_setup_theme', 'museum_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function museum_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'museum' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'museum_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function museum_scripts() {
	wp_enqueue_style( 'museum-style', get_stylesheet_uri() );

	wp_enqueue_script( 'vendor-ajajs', get_template_directory_uri() . '/app/lib/aja.min.js', array(), '20151215', false );
	wp_enqueue_script( 'vendor-dynamics', get_template_directory_uri() . '/app/lib/dynamics.min.js', array(), '20151215', true );
	wp_enqueue_script( 'museum-navigation', get_template_directory_uri() . '/app/navigation.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'museum_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

define('MUSEE__API_KEY', get_option('musee_api_key', 'AIzaSyBkL9syKDX-e_Rl3gca_J08Qswjubcm7zo'));

if (get_option('musee_hide_config', 'off') === 'off') {
	add_action('admin_menu', 'musee_menu');
	function musee_menu(){
		add_menu_page('Configuration Thème', 'Configuration Thème', 'manage_options', 'gestion_immo', 'musee_render_panel_admin', 'dashicons-admin-settings', '6');
		// add_submenu_page('admin.php?page=gestion_immo', 'Configuration', 'manage_options');
	};

	function musee_render_panel_admin(){
		require get_template_directory() . '/config/panel.config.php';
	};

	add_action( 'admin_enqueue_scripts', 'musee_config_script');
	function musee_config_script($hook){
		// if ('post.php' != $hook && 'post-new.php' != $hook) {
		// 	return;
		// }


		/**
		* Google Place API
		*/
		//  Ne pas oublier d'ajouter async defer
		// Permet d'enlever ?ver=2.. que rajoute wordpress à chaque fichiers et qui empêche
		// le bon fonctionnement de la requête vers google.

		wp_register_script('musee_autocomplete_script', get_template_directory_uri().'/app/autocomplete.js', dirname(__FILE__));
		wp_enqueue_script('musee_autocomplete_script');
		wp_localize_script('musee_autocomplete_script', 'AjaxAutocomplete', array(
			'key' => MUSEE__API_KEY
		));
	};

}else{

}


if (get_option('musee_option_light_js', 'false') === 'true'){
	/**
	* my_deregister_scripts | Permet de virer tous les chargements de fichiers JS à la con et à priori inutiles
	*/
	function my_deregister_scripts(){
		wp_deregister_script( 'wp-embed' );
	}
	add_action( 'wp_footer', 'my_deregister_scripts' );

	/**
	* Permet de retirer le chargement du pack emoji release, javascript du header
	*/
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );

	/**
	* Désactive la barre admin quand un user est connecté
	*/
	add_filter('show_admin_bar', '__return_false');
}

# Definition d'une constante permettant de masquer ou non les mises à jours de Wordpress
if (get_option('musee_update_hide', 'false') === 'true') {
	function remove_core_updates(){
		global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
	}
	add_filter('pre_site_transient_update_core','remove_core_updates');
	add_filter('pre_site_transient_update_plugins','remove_core_updates');
	add_filter('pre_site_transient_update_themes','remove_core_updates');
}

# Definition du nombre de revisions pour les post de Wordpress, mettre à false pour desactiver cette option
define('WP_POST_REVISIONS', get_option('musee_revisions', 3));



/**
 * Filter the excerpt "read more" string.
 * Permet d'ajouter le lien vers l'article 'Lire la suite...' de manière automatique et propre
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
	global $post;
    return '... <a href="'. esc_url( get_permalink()).'">'. get_option('musee_read_more', 'Lire la suite.') .'</a>';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );


/**
 * [pagination | Permet de mettre en place la pagination numérique]
 * @param  string  $pages [description]
 * @param  integer $range [description]
 * @return [type]         [description]
 */
function pagination($pages = '', $range = 4)
{
     $showitems = ($range * 2)+1;

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }

     if(1 != $pages)
     {
		 echo '<div class="pagination"><ul>';
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."#actualites'>&laquo; First</a></li>";
         if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."#actualites'>&lsaquo; Previous</a></li>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<li class=\"is-current\"><span class=\"current\">".$i."</span></li>":"<li><a href='".get_pagenum_link($i)."#actualites' class=\"inactive\">".$i."</a></li>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<li><a href=\"".get_pagenum_link($paged + 1)."#actualites\">Next &rsaquo;</a></li>";
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."#actualites'>Last &raquo;</a></li>";
		 echo '</ul></div>';

     }
}
