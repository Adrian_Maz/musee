/**
 * Fichier permettant de géocoder l'adresse rentrer dans le champ 'Adresse du marqueur' par l'utilisateur
 * Pour récupérer via le geocoder la latitude et la longitude du marqueur, puis d'entrer les données récupérées
 * dans les champs prévus à cet effet
 */


 /**
     * Fonction permettant de charger le script de l'API Google Place
 */
 function musee_load_place(src, callback){
     // Callback en option, si besoin, peut être supprimer.
     var script = document.createElement("script");
     script.type = "text/javascript";
     script.defer = true;
     script.async = true;

     if(callback)script.onload=callback;
     document.getElementsByTagName("head")[0].appendChild(script);
     script.src = src;
 };
 musee_load_place('https://maps.googleapis.com/maps/api/js?key=' + AjaxAutocomplete.key + '&libraries=places&callback=initAutocomplete', function(){});


 function initAutocomplete() {
   // Create the autocomplete object, restricting the search to geographical
   // location types.
   autocomplete = new google.maps.places.Autocomplete(
       /** @type {!HTMLInputElement} */(document.getElementById('musee_option_map_autocomplete')),
       {types: ['geocode']});

   // When the user selects an address from the dropdown, populate the address
   // fields in the form.
   autocomplete.addListener('place_changed', fillInAddress);
};

function fillInAddress() {
    // Get the place details from the autocomplete object.
    // Return object
    var place = autocomplete.getPlace();

    /**
     * Récupération de la latitude et de la longitude de l'adresse entrée
     */
    var markerLatitude = place.geometry.location.lat();
    var markerLongitude = place.geometry.location.lng();

    /**
     * Changement automatique des values des inputs avec la latitude et la longitude obtenu précédemment
     */
    var inputLatitude = document.querySelector('#musee_option_map_lat');
    var inputLongitude = document.querySelector('#musee_option_map_lng');

    inputLatitude.value = markerLatitude;
    inputLongitude.value = markerLongitude;
}

/**
 * [preventSend | Permet de desactiver l'envoi global du formulaire quand l'utilisateur frappe la touche entrée alors qu'il est dans le champ]
 */
 function preventSend(){
     var inputAutoComplete = document.querySelector('#musee_option_map_autocomplete');

     /**
      * Permet de desactiver l'envoi global du formulaire quand l'utilisateur frappe la touche entrée alors qu'il est dans le champ
      * autocomplete
      */
     inputAutoComplete.addEventListener('keypress', function(e) {
         if (e.keyCode === 13) {
                e.preventDefault();
         }
     });
 };

window.addEventListener('load', preventSend);
