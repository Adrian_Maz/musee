'use strict'

var menu = document.querySelector


/**
 * Class Todo, permet de créer et de gérer l'objet Todo
 * New pretty or sugar syntax for old constructor function
 */
class Todo{
    /**
     * [constructor | retur object]
     * @param  {[string]} title       [title of task]
     * @param  {[string]} description [description of task]
     * @return {[object]}             [Create todo object instanciable]
     */
    constructor(title, description){
        this.title = title;
        this.description = description;
        this.date = this.getCurrentDate();
        this.isDone = false;
    }

    /**
     * [getCurrentDate | Get current date]
     * @return {[string]} [Current Hours:Minutes i.e : '12h38']
     */
    getCurrentDate(){
        let date = new Date();
        let hours = date.getHours();
        let mins = date.getMinutes();

        // return hours + ':' + mins;
        return `${hours}:${mins}`; // New concatenation of string and variable with ECMA 6
    }
}


let task1 = new Todo('string', 'description');
console.log(task1)
