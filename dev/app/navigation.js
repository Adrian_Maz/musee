/**
 * Navigation.js
 * For toggle navigation for small screen, and support tab index navigation
 */
(function(){

	aja()
	    .method('get')
	    .url('http://localhost/musee//wp-json/wp/v2/posts')
	    .on('success', function(data){
			console.log(data);
	        //data is a javascript object
	    })
	.go();

	var windowWidth = window.innerWidth;
	var body = document.querySelector('body');
	var siteNavigation = document.querySelector('#siteNnavigation');
	var hamburger = siteNavigation.querySelector('#hamburger');
	var closeButton = siteNavigation.querySelector('#close');
	var overlay = document.querySelector('.overlay');
	var menuContainer = siteNavigation.querySelector('.menu-primary-container');

	console.log(windowWidth);

	hamburger.addEventListener('click', function(){
		if (windowWidth < 640) {
			console.log('Function small');
			openNavigationSmall();
		}else{
			console.log('Function Regular');
			openNavigationRegular();
		}
	});


	function openNavigationSmall(){
		dynamics.animate(hamburger, {
			scale: 0
		},{
			type: dynamics.easeInOut,
			friction: 200,
			duration: 250,
			complete: function(){
				dynamics.css(hamburger, {
					visibility: 'hidden'
				})
			}
		});

		dynamics.css(closeButton, {
			display: 'block'
		})
		dynamics.animate(closeButton, {
			scale: 1
		},{
			type: dynamics.easeInOut,
			friction: 200,
			duration: 250,
			delay: 250
		})

		dynamics.css(menuContainer, {
			translateX: window.innerWidth,
			display: 'block'
		})

		dynamics.animate(menuContainer, {
			translateX: 0
		},{
			type: dynamics.gravity,
			// friction: 200,
			duration: 500,
			delay: 250
		})

		closeButton.addEventListener('click', closeNavigationSmall);
	}

	function closeNavigationSmall(){
		dynamics.css(hamburger, {
			visibility: 'visible'
		})

		dynamics.animate(hamburger, {
			scale: 1
		},{
			type: dynamics.easeInOut,
			friction: 200,
			duration: 500,
			delay: 500
		});

		dynamics.animate(closeButton, {
			scale: 0,
		},{
			type: dynamics.easeInOut,
			friction: 200,
			duration: 500,
			complete: function(){
				dynamics.css(closeButton, {
					display: 'none'
				});
			}
		})


		dynamics.animate(menuContainer, {
			translateX: window.innerWidth
		},{
			type: dynamics.easeInOut,
			friction: 200,
			duration: 250,
			complete: function(){
				dynamics.css(menuContainer, {
					translateY: window.innerHeight,
					display: 'block'
				})
			}
		})
	}





	function openNavigationRegular(){
		dynamics.animate(hamburger, {
			scale: 0
		},{
			type: dynamics.easeInOut,
			friction: 200,
			duration: 250,
			complete: function(){
				dynamics.css(hamburger, {
					visibility: 'hidden'
				})
			}
		});

		dynamics.css(closeButton, {
			display: 'block'
		})
		dynamics.animate(closeButton, {
			scale: 1,
			translateY: - closeButton.offsetHeight / 2 - 175
		},{
			type: dynamics.easeInOut,
			friction: 200,
			duration: 250,
			delay: 250
		})

		dynamics.css(menuContainer, {
			translateY: -175 * 2,
			display: 'block'
		})

		dynamics.animate(menuContainer, {
			translateY: -menuContainer.offsetHeight
		},{
			type: dynamics.gravity,
			// friction: 200,
			duration: 500,
			delay: 250
		})


		dynamics.animate(body, {
			translateY: '175px'
		}, {
			type: dynamics.easeInOut,
			friction: 200,
			duration: 500,
		})

		closeButton.addEventListener('click', closeNavigationRegular);

		dynamics.css(overlay, {
			display: 'block',
			opacity: 0
		})

		dynamics.animate(overlay, {
			opacity: 0.6
		},{
			type: dynamics.easeIn,
			friction: 200,
			duration: 500
		})

		overlay.addEventListener('click', closeNavigationRegular);
	}

	function closeNavigationRegular(){
		dynamics.css(hamburger, {
			visibility: 'visible'
		})

		dynamics.animate(hamburger, {
			scale: 1,
			translateY: - hamburger.offsetHeight / 2
		},{
			type: dynamics.easeInOut,
			friction: 200,
			duration: 500,
			delay: 500
		});

		dynamics.animate(closeButton, {
			scale: 0,
			translateY: - closeButton.offsetHeight / 2
		},{
			type: dynamics.easeInOut,
			friction: 200,
			duration: 500,
			complete: function(){
				dynamics.css(closeButton, {
					display: 'none'
				});
			}
		})

		dynamics.animate(overlay, {
			opacity: 0
		},{
			tyoe: dynamics.easeIn,
			friction: 200,
			duration: 500,
			complete: function(){
				dynamics.css(overlay, {
					display: 'none'
				})
			}
		})

		dynamics.animate(body, {
			translateY: 0
		},{
			type: dynamics.easeIn,
			friction: 200,
			duration: 500
		})


		dynamics.animate(menuContainer, {
			translateY: -menuContainer.offsetHeight
		},{
			type: dynamics.easeInOut,
			friction: 200,
			duration: 250,
			complete: function(){
				dynamics.css(menuContainer, {
					translateY: -menuContainer.offsetHeight,
					display: 'block'
				})
			}
		})
	}


}());
