<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package museum
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <section class="title-intro">
        <h1>Musee <span>Auto Moto</span> <br>de Chatellerault</h1>
        <div class="overlay"></div>
    </section>
    <section class="section-slider">
        <div class="slider">
            <img src="<?php bloginfo() ?>" alt="" />
        </div>
    </section>

    <nav id="siteNnavigation" class="main-navigation" role="navigation">
        <button class="container-hamburger" aria-controls="primary-menu" id="hamburger" aria-expanded="false">
            <span class="hamburger" aria-controls="primary-menu" aria-expanded="false"></span>
        </button>
        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
        <div class="close" id="close">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="357px" height="357px" viewBox="0 0 357 357" style="enable-background:new 0 0 357 357;" xml:space="preserve">
                <g id="close">
                    <polygon points="357,35.7 321.3,0 178.5,142.8 35.7,0 0,35.7 142.8,178.5 0,321.3 35.7,357 178.5,214.2 321.3,357 357,321.3     214.2,178.5   "/>
                </g>
            </svg>
        </div>
    </nav>

    <section class="content">
        <div class="wrap">
            <?php
                $idpresentation = 27;
                $args = array('page_id' => $idpresentation);
                $query = new WP_Query($args);
                if ($query->have_posts()) :
                    while($query->have_posts()) : $query->the_post(); ?>
                        <div class="presentation">
                            <h2><?php the_title(); ?></h2>
                            <p>
                                <?php the_content(); ?>
                            </p>
                        </div>
                <?php endwhile; ?>
            <?php endif; wp_reset_postdata();?>

            <div class="posts-preview-container" id="actualites">
                <h2>Actualités</h2>
                <?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args = array(
                    'post_type' => 'post',
                    'paged' => $paged,
                    'posts_per_page' => get_option('musee_posts_per_page', 3),
                );

                $query = new WP_Query($args);

                if ( $query->have_posts() ) :
                /* Start the Loop */
                while ( $query->have_posts() ) : $query->the_post();
                ?>

                    <article class="posts-preview">
                        <a href="<?php the_permalink();?>" class="preview">
                            <?php the_post_thumbnail(); ?>
                            <div class="more">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="457.03px" height="457.03px" viewBox="0 0 457.03 457.03" style="enable-background:new 0 0 457.03 457.03;" xml:space="preserve">
                                        <g>
                                            <path style="fill: #FFF" d="M421.512,207.074l-85.795,85.767c-47.352,47.38-124.169,47.38-171.529,0c-7.46-7.439-13.296-15.821-18.421-24.465   l39.864-39.861c1.895-1.911,4.235-3.006,6.471-4.296c2.756,9.416,7.567,18.33,14.972,25.736c23.648,23.667,62.128,23.634,85.762,0   l85.768-85.765c23.666-23.664,23.666-62.135,0-85.781c-23.635-23.646-62.105-23.646-85.768,0l-30.499,30.532   c-24.75-9.637-51.415-12.228-77.373-8.424l64.991-64.989c47.38-47.371,124.177-47.371,171.557,0   C468.869,82.897,468.869,159.706,421.512,207.074z M194.708,348.104l-30.521,30.532c-23.646,23.634-62.128,23.634-85.778,0   c-23.648-23.667-23.648-62.138,0-85.795l85.778-85.767c23.665-23.662,62.121-23.662,85.767,0   c7.388,7.39,12.204,16.302,14.986,25.706c2.249-1.307,4.56-2.369,6.454-4.266l39.861-39.845   c-5.092-8.678-10.958-17.03-18.421-24.477c-47.348-47.371-124.172-47.371-171.543,0L35.526,249.96   c-47.366,47.385-47.366,124.172,0,171.553c47.371,47.356,124.177,47.356,171.547,0l65.008-65.003   C246.109,360.336,219.437,357.723,194.708,348.104z"/>
                                        </g>
                                    </svg>
                                </span>
                            </div>
                        </a>
                        <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                            <?php the_excerpt(); ?>

                    </article>
                <?php
                endwhile;
                    if (function_exists("pagination")) {
                        pagination($query->max_num_pages);
                    }
                endif;
                wp_reset_postdata();
                ?>
            </div>

        </div><!-- #main -->
    <!-- </section> -->
<?php
get_sidebar();
get_footer();
