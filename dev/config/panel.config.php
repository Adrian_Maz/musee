<?php
/**
    * Fichier assurant la vue, l'affichage du panneau de configuration
 */

 if (isset($_POST['update_panel'])) {
     # Si le champ hidden update panel est envoyé en POST, cela signifie que l'utilisateur à bien envoyé le formulaire

    /**
    * Petit script qui boucle sur les secteurs si il y en a
    * qui les stock dans un tableau puis fusionne ce tableau pour en faire une chaine de caractère
    * prête a être uploader ce qui est plus perfomant.
    */

    # Toutes les options autres que type de bien, secteur, tranche prix location, tranche prix de vente.
    # Cela concerne les options, prefix, champ date d'expiration, champ equipement etc...
    foreach ($_POST['musee_options'] as $key => $option) {
        if (empty($option)){
            delete_option($key);
        }else{
            update_option($key, $option);
        }
    };
    echo '<div class="updated"><p>Paramètres enregistrés avec succès !</p></div>';
};
?>
<style media="screen">
    .card{
        float: left;
        margin-right: 10px;
    }

    hr{
        border: none;
        height: 1px;
        background: rgba(0, 0, 0, 0.1);
        margin: 20px 0 10px 0;

    }

    span.description{
        display: block;
        width: 95%;
        margin: 10px auto;
        color: rgba(0, 0, 0, 0.7);
    }

    label{
        display: block;
        font-size: 1.1em;
        font-weight: bold;
        padding-bottom: 5px;
        margin-top: 25px;
    }

    label:nth-child(1){
        margin-top: 0px;
    }

    input{
        height: 40px;
        font-size: 1.1em;
    }

    input:focus{
        color: #2196F3;
    }

    #secteurs_inputs_container, #type_inputs_container, #pieces_inputs_container{
        width: 90%;
        padding: 10px;
        margin: 0 auto;

        background: rgba(0, 0, 0, 0.05);
    }
</style>
<h1>Configuration</h1>
<p>
    Panneau servant à configurer les différents champs de la metabox.
</p>

<div class="card pressthis">
    <form class="" action="" method="post">
        <div class="form-field form-required">

            <h2>Configuration</h2>
            <!-- <label for="musee_option_prefix">Prefix</label> -->
            <!-- <input type="text" id="musee_option_prefix" name="musee_options[musee_prefix]" value="<?php echo get_option('musee_prefix', 'emusee_'); ?>"> -->
            <!-- <br>
            <span class="description">
                - Il faut s'assurer que les meta seront <strong>uniques</strong> et ne peuvent rentrer en collision avec aucun autre plugin/extenstion déjà installé.
                <br>- Sélectionnez et remplacez ce texte si besoin, sinon <strong>laissez le par défaut sur "emusee_"</strong>.
                <br>- Enfin ce champ <strong>ne doit pas être changés après que des données ont été enregistrées</strong> sinon ces données seront dans la bdd avec l'ancien préfix ne seront plus accessibles.
            </span> -->



            <label for="musee_option_year_construct">Infos Pratiques - Footer</label>
            <?php for ($i=0; $i < 3; $i++) : ?>
            <select class="" id="musee_option_year_construct" name="musee_options[musee_footer_<?php echo $i; ?>]">
                <?php
                    $args = array('post_type' => 'page');
                    $query = new WP_Query($args);
                    if($query->have_posts()) :
                        while($query->have_posts()) : $query-> the_post()
                ?>
                    <?php if (get_option('musee_footer_'. $i, '0') == get_the_ID() ): ?>
                        <option value="<?php the_ID(); ?>" selected><?php the_title(); ?></option>
                    <?php else: ?>
                        <option value="<?php the_ID(); ?>"><?php the_title(); ?></option>
                    <?php endif; ?>
            <?php
                    endwhile;
                endif;
                wp_reset_postdata();
             ?>
            </select><br><br>
            <?php endfor; ?>
            <span class="description">
                - Ajouter ici les trois pages qui composeront le footer <br>
                - L'ordre sera le même que celui des champs
            </span>

            <label for="musee_read_more">Lien pour lire les articles</label>
            <input type="text" id="musee_read_more" name="musee_options[musee_read_more]" value="<?php echo get_option('musee_read_more', 'Lire la suite.'); ?>" min="0" max="7" onclick="select()">
            <span class="description">
                - Entrez ici le lien que verront les utilisateurs à la suite de l'extrait de chacun de vos articles.
            </span>
            <hr>

            <label for="musee_posts_per_page">Nombre d'articles par page</label>
            <input type="number" id="musee_posts_per_page" name="musee_options[musee_posts_per_page]" value="<?php echo get_option('musee_posts_per_page', '3'); ?>" min="0" max="7" onclick="select()">
            <span class="description">
                <strong>- Entrez le nombre d'articles visibles par page.</strong> <br>
                - Par défaut : 3
            </span>
            <hr>

            <label for="musee_option_nb_revisions">Nombre de révisions possible</label>
            <input type="number" id="musee_option_nb_revisions" name="musee_options[musee_revisions]" value="<?php echo get_option('musee_revisions', '3'); ?>" min="0" max="7" onclick="select()">
            <span class="description">
                - Permet de limiter le nombre sauvegarde automatique des articles et par conséquent l'espace prit par cette fonctionnalité dans la BDD <br>
                - Entrez le nombre de révisions d'articles possibles.<br>
                - Par défaut : 3
            </span>
            <hr>


            <label for="musee_option_update_hide">Masquer les mises à jours</label>
            <select class="" id="musee_option_update_hide" name="musee_options[musee_update_hide]">
                <?php if (get_option('musee_update_hide', 'false') === 'true'): ?>
                    <option value="true" selected>Oui</option>
                    <option value="false">Non</option>
                <?php else: ?>
                    <option value="true">Oui</option>
                    <option value="false" selected>Non</option>
                <?php endif; ?>
            </select>
            <span class="description">
                - Permet de masquer ou non les notifications de mises à jorus de WordPress, ainsi que des extensions
            </span>
            <hr>


            <h2>Masquer le panneau de configuration</h2>
            <label for="musee_hide_config"></label>
            <select class="" id="musee_hide_config" name="musee_options[musee_hide_config]">
                <?php if (get_option('musee_show_config', 'off') === 'on'): ?>
                    <option value="off">Non</option>
                    <option value="on" selected>Oui</option>
                <?php else: ?>
                    <option value="off" selected>Non</option>
                    <option value="on">Oui</option>
                <?php endif; ?>
            </select>
            <span class="description">
                - Une fois 'Oui' sélectionné et enregistré ce panneau <strong>ne sera plus visible depuis l'interface d'administration de wordpress.</strong><br><br>
                - Cependant en cas de necessité il peut être à nouveau affiché manuellement en mettant <strong>off</strong> au champ <strong><i>musee_show_config</i></strong> dans la base de données table <strong><i>wp_options</i></strong>.<br><br>
                <strong>- Assurez vous de bien avoir tout configuré à votre souhait avant de masquer ce panneau.</strong>
            </span>
            <hr>


            <input type="hidden" name="update_panel" value="now">
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Enregistrer les modifications">
        </div>
    <!-- </form> -->
</div>

<!-- Configuration Plugin -->


<div class="card pressthis">
        <div class="form-field form-required">


            <h2>Light Javascript Engine</h2>
            <label for="musee_option_light_js"></label>
            <select class="" id="musee_option_light_js" name="musee_options[musee_option_light_js]">
                <?php if (get_option('musee_option_light_js', 'false') === 'true'): ?>
                    <option value="true" selected>Oui</option>
                    <option value="false">Non</option>
                <?php else: ?>
                    <option value="true">Oui</option>
                    <option value="false" selected>Non</option>
                <?php endif; ?>
            </select>
            <span class="description">
                - Permet de desactiver le chargement de scripts natif de Wordpress <strong>allégeant considérablement</strong> ses performances. <br>
                <strong> - Attention bien que ses scripts soient optionnels certains vous servent peut être.</strong>
            </span>
            <hr>

        </div>
</div>

        <div class="card pressthis">
            <div class="form-field form-required">


                <h2>Map</h2>
                <label for="musee_option_map_autocomplete">Adresse du marqueur</label>
                <?php if (!get_option('musee_map_address')): ?>
                    <input type="text" class="" id="musee_option_map_autocomplete" name="musee_options[musee_map_address]" onclick="select()">
                <?php else: ?>
                    <input type="text" class="" id="musee_option_map_autocomplete" name="musee_options[musee_map_address]" onclick="select()" value="<?php echo get_option('musee_map_address'); ?>">
                <?php endif; ?>
                <span class="description">
                    <strong> - Entrez l'adresse du marqueur qui sera visible sur les google maps</strong>
                </span>
                <hr>

                <label for="musee_option_map_lat">Map - Latitude</label>
                <?php if (!get_option('musee_map_lat')): ?>
                    <input class="" type="text" id="musee_option_map_lat" name="musee_options[musee_map_lat]">
                <?php else: ?>
                    <input class="" type="text" id="musee_option_map_lat" name="musee_options[musee_map_lat]" value="<?php echo get_option('musee_map_lat'); ?>">
                <?php endif; ?>

                <label for="musee_option_map_lng">Map - Longitude</label>
                <?php if (!get_option('musee_map_lng')): ?>
                    <input class="" type="text" id="musee_option_map_lng" name="musee_options[musee_map_lng]">
                <?php else: ?>
                    <input class="" type="text" id="musee_option_map_lng" name="musee_options[musee_map_lng]" value="<?php echo get_option('musee_map_lng'); ?>">
                <?php endif; ?>

                <hr>


                <input type="submit" name="submit" id="submit" class="button button-primary" value="Enregistrer les modifications">
            </div>
        </div>
    </form>
