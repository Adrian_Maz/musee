<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package museum
 */

?>
	</section>
	<footer>
		<div class="footer-coord">
			<?php
				$idfirst = get_option('musee_footer_0', '0');
				$args = array('page_id' => $idfirst);
				$query = new WP_Query($args);

				if ($query -> have_posts()) :
					while($query -> have_posts()) : $query -> the_post() ?>

					<?php the_title(); ?>
					<div class="map" id="map"></div>
					<div class="grid-6">
						<?php the_content(); ?>
					</div>

			<?php
					endwhile;
				endif;
				wp_reset_postdata();
			?>
		</div>

		<div class="footer-horaires">
			<?php
				$idsecond = get_option('musee_footer_1', '0');
				$args = array('page_id' => $idsecond);
				$query = new WP_Query($args);

				if ($query -> have_posts()) :
					while($query -> have_posts()) : $query -> the_post() ?>

					<?php the_title(); ?>
					<?php the_content(); ?>

			<?php
					endwhile;
				endif;
				wp_reset_postdata();
			?>
		</div>

		<div class="footer-mentions">
			<?php
				$idthird = get_option('musee_footer_2', '0');
				$args = array('page_id' => $idthird);
				$query = new WP_Query($args);

				if ($query -> have_posts()) :
					while($query -> have_posts()) : $query -> the_post() ?>

					<?php the_title(); ?>
					<?php the_content(); ?>

			<?php
					endwhile;
				endif;
				wp_reset_postdata();
			?>
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>
