<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'musee');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données.
  * N'y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'PhVdp*2WFsRe#?ZA{8;f*O)cwp_iL/Q|~hWv^X pH6h;na$aJyoQDPU5mLf_QaZ+');
define('SECURE_AUTH_KEY',  ';^GJq?x$MM.D -gb6ElG3J86RhBQP%cCD*?]Sfstw@b73P*2442,UP]V7P>]M3i)');
define('LOGGED_IN_KEY',    'x<j #`w(ID1W77b+]X2f Lyyj2h#{0%f~}4O<~Zh3[O$Gp-+Jtmpzvk?JkQp4<##');
define('NONCE_KEY',        '9} >c=?9O@y]wCWN&w`9i*RI[W7DHhwoDJB2vXFPCH|I1KDIYKWGZ8PZ4LKM3NZc');
define('AUTH_SALT',        '?m163>?R1#lI*keuIDfWW^8b;M;.CO0cHihp,++HP:ddM1}QcVt*X/2o4AL37Yp ');
define('SECURE_AUTH_SALT', 'Vfm3=%vQnk+EP4K6lSR@Vg<aQS(^-_l>`<R4x1vcJc^NXVa@Ev%jL):!^jGv/lU|');
define('LOGGED_IN_SALT',   'W4_#tT=q#R{Jh7{-bk~wnXH/^/lA]zkK*o=LP#Eff;*=hme%tlx&#c?.qNY{L.pY');
define('NONCE_SALT',       '5<R%Z@!_r;N@nD,wX[L:Vp[[|A4Jp$v5ArMZXEf?`T]gI_,ogP9p&/(8y^.Gqftd');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 */
define('WP_DEBUG', false);

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
