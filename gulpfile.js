'use strict';
/** [require | Require modules] */
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var coffee = require('gulp-coffee');
var jade = require('gulp-jade');

/**
 * Gulpfile Spécial pour mes démos !! ;)
 */

/**
    * --------------------------
    * [folders | relative path for foldersss | D means destination]
    * @type {Object}
    * --------------------------
 */
var folders = {

        sass : 'dev/sass/**/*.sass',
        scss : 'dev/sass/**/*.scss',
        cssD : 'wp-content/themes/museum/',

        html : 'dev/**/*.html',
        htmlD : 'wp-content/themes/museum/',

        jade : 'dev/*.jade',
        jadeD : 'wp-content/themes/museum/',

        js : 'dev/app/**/*.js',
        jsD : 'wp-content/themes/museum/app/',

        coffee : 'dev/app/**/*.coffee',
        coffeeD : 'wp-content/themes/museum/app/',

        img : 'dev/img/**/*.*',
        imgD : 'wp-content/themes/museum/img/',

        jsLib : 'dev/app/lib/**/*.js',
        jsLibD : 'wp-content/themes/museum/app/',

        php: 'dev/**/*.php',
        phpD: 'wp-content/themes/museum/',

        fonts: 'dev/fonts/*.*',
        fontsD: 'wp-content/themes/museum/fonts/'

}

/**
    * --------------------------
    * Tasks
    * --------------------------
 */

/**
    * Build
 */
gulp.task('default', ['php', 'sass', 'images', 'compress', 'copyjslib', 'fonts']);


/** [SASS | LibSass - Convert Scss to Css and move to Dist] */
gulp.task('sass', function(){
    return gulp.src(folders.sass)
        .pipe(sass({outputStyle: 'nested'}).on('error', sass.logError))
        .pipe(autoprefixer({
			browsers: ['> 1%', 'Last 5 versions','Firefox > 20','IE 8'],
			cascade: false
		}))
        .pipe(gulp.dest(folders.cssD));
});

/**
 * [php | Copy PHP files from Dev to Themes/museum folder]
 */
gulp.task('php', function(){
    return gulp.src(folders.php)
        .pipe(gulp.dest(folders.phpD));
});

/** [Compress - Compress Javascripts] */
gulp.task('compress', function(){
    return gulp.src(folders.js)
        // .pipe(uglify({mangle: false}))
        .pipe(gulp.dest(folders.jsD));
});

/** [Jade - Convert jade to html and past to dist folder] */
gulp.task('jade', function(){
    return gulp.src(folders.jade)
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest(folders.jadeD));
});

/** [Coffee - Convert coffee to js and past to dist folder] */
gulp.task('coffee', function(){
    return gulp.src(folders.coffee)
        .pipe(coffee({bare: true}))
        .pipe(gulp.dest(folders.jsD));
});

/** [copyjsvendor - Permet de copier toutes les librairies JS depuis Dev to Dist] */
gulp.task('copyjslib', function(){
    return gulp.src(folders.jsLib)
        .pipe(gulp.dest(folders.jsLibD));
});

/** [Html - Copie tous les .html depuis Dev vers Dist] */
gulp.task('html', function(){
    return gulp.src(folders.html)
        .pipe(gulp.dest(folders.htmlD));
});

/** [Json - Copie tous les .JSON depuis Dev vers Dist] */
gulp.task('json', function(){
    return gulp.src(folders.json)
        .pipe(gulp.dest(folders.jsonD));
});

/** [AnimImg - Copie toutes les images nécessaires aux animations dans le folder dist] */
gulp.task('animImg', function(){
    return gulp.src(folders.animImg)
        .pipe(gulp.dest(folders.animImgD));
});

/** [Assets - Copie tout les assets from dev to dist folders {fonts, img, etc...}] */
gulp.task('images', function(){
    return gulp.src(folders.img)
        .pipe(gulp.dest(folders.imgD));
});

/** [fonts - Copie les fonts] */
gulp.task('fonts', function(){
    return gulp.src(folders.fonts)
        .pipe(gulp.dest(folders.fontsD));
});



/**
    * --------------------------
    * Watching Tasks
    *  - La tache 'live' permet de regrouper différentes tâches watch en une seule et unique,
    *  ainsi que de renvoyer dans la console, un log avec l'heure de modif
    *  de chaque fichier ainsi que les possibles erreurs.
    *
    *  Elle est surtout destinée pour la modification du code
    *  pas pour des assets, tel que les images, svg, ou vendors,
    *  ce qui la rendrait bien plus lente et moins performante.
    * --------------------------
 */

gulp.task('live', ['sass', 'php', 'compress'], function(){
    gulp.watch(folders.sass, ['sass']).on('change', function(event){
        console.log('Le fichier :' + event.path  + ' a ete modifie !')
    });
    gulp.watch(folders.php, ['php']).on('change', function(event){
        console.log('Le fichier :' + event.path + ' a ete modifie !')
    });
    gulp.watch(folders.js, ['compress']).on('change', function(event){
        console.log('Le fichier :' + event.path + ' a ete modifie !')
    });
});
